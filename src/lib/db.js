import mariadb from 'mariadb';

const DB = mariadb.createPool({
    host: import.meta.env.VITE_DBURL, 
    user: import.meta.env.VITE_DBUSER,
    password: import.meta.env.VITE_DBPASSWORD,
    database: import.meta.env.VITE_DBNAME,
    connectionLimit: 5
});

export default DB;
