import DB from "$lib/db.js";

// PATCH /todos/:uid.json
export const patch = async (req) => {
    let conn;
    let response;
    let insertId;
    
    try {
        conn = await DB.getConnection();
        response = await conn.query(
            "INSERT INTO docs VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
            [null, req.body.get('title'), req.body.get('text'), 1, req.body.get('timestamp'), req.params.slug, req.body.get('theme'), req.body.get('tags')]
          );
        insertId = response.insertId;
        response = await conn.query("UPDATE `docs` SET `visible` = 0 WHERE id = "+req.params.slug);
    } catch (err) {
        throw err;
    } finally {
        if (conn) conn.end();

        return {
            status: 200,
            body: {
                id: insertId,
                title: req.body.get('title'), 
                text: req.body.get('text'), 
                visible: 1, 
                timestamp: req.body.get('timestamp'), 
                previous_version: null, 
                theme: Number(req.body.get('theme')), 
                tags: req.body.get('tags')
            }
        }
    }
}

// DELETE /docs/:uid.json
export const del = async (req) => {
    let conn;
    let response;
    
    try {
        conn = await DB.getConnection();
        response = await conn.query("UPDATE `docs` SET `visible` = 0 WHERE id = "+req.params.slug);
    } catch (err) {
        throw err;
    } finally {
        if (conn) conn.end();

        return {
            status: 200,
            body: {
                data: 'success'
            }
        }
    }
};