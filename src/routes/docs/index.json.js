import DB from "$lib/db.js";

export const get = async (request) => {
    let conn;
    let docs = [];
    let doc = null;
    const id = request.query.get('id');
    
    try {
        conn = await DB.getConnection();
        if (id) {
            doc = await conn.query(`select * from docs where id=${id}`);
            doc = doc[0];
        } else {
            docs = await conn.query("select * from docs where visible=1");
        }

    } catch (err) {
        throw err;
    } finally {
        if (conn) conn.end();
                
        if (doc) {
            return {
                status: 200,
                body: {
                    doc
                }
            }
        } else {
            return {
                status: 200,
                body: {
                    docs
                }
            }
        }
    }
}

export const post = async (req) => {
    let conn;
    let response;
    let insertId;
    
    try {
        conn = await DB.getConnection();
        response = await conn.query(
            "INSERT INTO docs VALUES (?, ?, ?, ?, ?, ?, ?, ?)", 
            [ null, req.body.get('title'), req.body.get('text'), true, req.body.get('timestamp'), null, Number(req.body.get('theme')), req.body.get('tags')]
          );
        insertId = response.insertId;
    } catch (err) {
        throw err;
    } finally {
        if (conn) conn.end();


        return {
            status: 200,
            body: {
                id: insertId,
                title: req.body.get('title'), 
                text: req.body.get('text'), 
                visible: 1, 
                timestamp: req.body.get('timestamp'), 
                previous_version: null, 
                theme: Number(req.body.get('theme')), 
                tags: req.body.get('tags')
            }
        };
    }
}
