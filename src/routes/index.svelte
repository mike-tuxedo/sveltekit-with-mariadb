<script context="module">
	export async function load({ page, fetch, session, context }) {
		return {
			props: {
				docs: context.data.docs
			}
		}
	}
</script>

<script>
    export let docs = [];

	function enhance(form, { pending, error, result }) {
		async function handle_submit(e) {
			e.preventDefault();

			const body = new FormData(form);
			
			try {
				const res = await fetch(form.action, {
					method: form.method,
					headers: {
						accept: 'application/json'
					},
					body
				});

				if (res.ok) {
					result(res, form);
				} else if (error) {
					error(res, null, form);
				} else {
					console.error(await res.text());
				}
			} catch (e) {
				if (error) {
					error(null, e, form);
				} else {
					throw e;
				}
			}
		}

		form.addEventListener('submit', handle_submit);

		return {
			destroy() {
				form.removeEventListener('submit', handle_submit);
			}
		};
	}
	$: console.log(docs);
</script>

<main class="font-sans bg-green-400">
	<form 
		action="/docs.json" 
		method="post" 
		use:enhance={{result: async (res) => {
			let data = await res.json();
			docs = [...docs, data];
		}}}
	>
		<input type="text" name="title" value="test"/>
		<input type="text" name="text" value="test"/>
		<input type="text" name="theme" value="3"/>
		<input type="text" name="tags" value="[1,2,3]"/>
		<input type="text" name="timestamp" value={Date.now()}/>
		<input type="submit" value="add"/>
	</form>

	<ul class="list">
	{#each docs as doc}
		<li class="listitem">
			<h2>{doc.id} {doc.title}</h2>
			<div>{@html doc.text}</div>

			<form
				action="/docs/{doc.id}.json?_method=patch"
				method="post"
				use:enhance={{result: async (res) => {
					let data = await res.json();
					docs = [...docs.filter(cur => cur.id !== doc.id), data];
				}}}
			>
				<input type="text" name="title" value={doc.title}/>
				<input type="text" name="text" value={doc.text}/>
				<input type="text" name="timestamp" value={doc.timestamp}/>
				<input type="text" name="theme" value={doc.theme}/>
				<input type="text" name="tags" value={doc.tags}/>
				<button class="patch" on:click={this.submit}>Patch</button>
			</form>

			<form
				action="/docs/{doc.id}.json?_method=delete"
				method="post"
				use:enhance={{result: (response) => {
					if (response.ok) docs = docs.filter(cur => cur.id !== doc.id);
				}}}
			>
				<button class="delete" on:click={this.submit}>Delete</button>
			</form>
		</li>
	{/each}
	</ul>
</main>

<style>
	main {
		font-family: Arial, Helvetica, sans-serif;
	}
	ul {
		margin: 0;
		padding: 0;
		box-sizing: border-box;
	}
	li {
		border-bottom: 1px solid #000;
	}
	form {
		padding: 20px;
	}
	form:first-child {
		border-bottom: 1px solid #000;
	}
	input {
		padding: 10px;
	}
</style>